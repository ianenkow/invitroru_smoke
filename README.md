# Project to learn python automation testing.

All required packages in *requirements.txt* file, to download it use command (don't forget to activate virtual env first)

`pip install -r /path/to/requirements.txt`

You can update all packages after installation with command:

`pip install -r requirements.txt --upgrade`

Or use google for another way

Due to pageobject model all:
- locators are in *locators* directory
- methods for every page are in *pages* directory
- tests are in *tests* directory (used one file for all smoke tests)

Locators distributed between header, footer, elements to interact and elements to check visibility.
Also locators' directory *expected_values.py* file to keep expected values for test

There are also *conftest.py* file with methods for pytest and *pytest.ini* file for pytest markers
in root directory

Every test execution screenshot is made, it'll be in *screenshots* directory after execution.

To run tests go to *tests* directory and use command

`pytest test_smoke.py`

File *pytest.ini* contains markers for pytest. Only 'current' mark is available and was
used for debugging. To execute one (or several) tests:
- type `@pytest.mark.current` before title of test (befor `def`)
- use command `pytest -m current test_smoke.py`

*pytest.ini* is not just a file to keep markers, you can configurate pytest with different settings
in this file, check pytest documentation.
