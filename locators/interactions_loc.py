from selenium.webdriver.common.by import By

#Covid19 page
TITLES_ELEMENTS = (By.CSS_SELECTOR, '.test-title h2')

#catalog page
TEST_GROUPS = (By.CSS_SELECTOR, '.side-bar__block:nth-child(2) .side-bar-second__items')
FIRST_PRODUCT_NUMBER = (By.CSS_SELECTOR, '.pagination-items:first-child .show-block-wrap:nth-child(2) .result-item__number > span')
FIRST_PRODUCT_CART_BUTTON = (By.XPATH, '(//div[contains(@class, "result-item__row")]//div[contains(@class, "btn-cart")]//a)[1]')

#Doctors page
DOCTOR_SPECIALIZATIONS = (By.CSS_SELECTOR, '.vrachi-specialty__item a')

#service page
SERVICES_GROUPS = (By.CSS_SELECTOR, '.side-bar-second__items')

#visit page
PHONE_NUMBER = (By.CSS_SELECTOR, '.vnd-phone')

#examination page
EXAMINATION_GROUPS = (By.CSS_SELECTOR, 'a[class ^= side-bar]')

#cart page
PRODUCT_NUMBERS = (By.XPATH, '//*[contains(@class, "CartProduct_productTags")]/span[1]')

