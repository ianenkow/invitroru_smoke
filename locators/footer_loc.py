from selenium.webdriver.common.by import By

PHONES = (By.CSS_SELECTOR, '#footerContacts a')
SITES_TITLES = (By.CSS_SELECTOR, '.ft-lg-list a')
GROUPS_TITLES = (By.CSS_SELECTOR, '#footerMenu a')
BUTTON_TITLES = (By.CSS_SELECTOR, '.invitro_footer-btn_part1 span:nth-child(2)')
