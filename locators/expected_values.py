covid19_titles = ['ПЦР-тесты', 'Тесты на антитела к коронавирусу SARS-COV-2', 'Комплексные исследования для диагностики COVID-19 и оценки состояния после перенесенной коронавирусной инфекции']

header_phones = ['8 (495) 363-0-363', '8 (800) 200-363-0']

footer_phones = ['8 (800) 200-363-0', '8 (495) 363-0-363', 'Обратная связь']
footer_sites = ['Пациентам', 'Врачам', 'Франчайзинг', 'Корпоративным клиентам', 'Прессе']
footer_groups = ['Медицинские услуги', 'Комплексные обследования', 'Запись к врачу', 'Выезд на дом', 'Адреса медицинских офисов', 'Библиотека пациента', 'Вопрос врачу', 'Об ИНВИТРО', 'Партнеры', 'Вакансии', 'Клиники-партнеры', 'Юридическая информация', 'Программа лояльности', 'Клинические исследования', 'Результаты СОУТ']
footer_buttons = ['Каталог анализов', 'Результаты анализов']