from selenium.webdriver.common.by import By

# main page chapters
POPULAR_TESTS_TITLE = (By.CSS_SELECTOR, '.main-rr-title')
MEDICAL_SERVICES_TITLE = (By.CSS_SELECTOR, '.main-medical-services__title')
INVITRO_DOCTORS_TITLE = (By.CSS_SELECTOR, '.dsec-slider__header .st-h2')
NEWS_TITLE = (By.CSS_SELECTOR, '.main-news .st-h2')

#COVID-19
PCR_FIRST_CARD_TITLE = (By.CSS_SELECTOR, '#ptsr-testy-mazok .test-cards_item:first-child .items-h4')
PCR_FIRST_CARD_PRICE = (By.CSS_SELECTOR, '#ptsr-testy-mazok .test-cards_item:first-child .test-cards_item-price')
PCR_FIRST_CARD_BOOKING_BUTTON = (By.CSS_SELECTOR, '#ptsr-testy-mazok .test-cards_item:first-child .test-cards_item-appointment a')
ANTIBODIES_FIRST_CARD_TITLE = (By.CSS_SELECTOR, '#testy-na-antitela-venoznaya-krov .test-cards_item:first-child .items-h4')
ANTIBODIES_FIRST_CARD_PRICE = (By.CSS_SELECTOR, '#testy-na-antitela-venoznaya-krov .test-cards_item:first-child .test-cards_item-price')
ANTIBODIES_FIRST_CARD_BOOKING_BUTTON = (By.CSS_SELECTOR, '#testy-na-antitela-venoznaya-krov .test-cards_item:first-child .test-cards_item-appointment a')
AFTERCOVID_FIRST_CARD_TITLE = (By.CSS_SELECTOR, '#kompleksnye-issledovaniya-dlya-diagnostiki-covid-19-i-otsenki-sostoyaniya-posle-perenesennoy-koronav .test-cards_item:first-child .items-h4')
AFTERCOVID_FIRST_CARD_PRICE = (By.CSS_SELECTOR, '#kompleksnye-issledovaniya-dlya-diagnostiki-covid-19-i-otsenki-sostoyaniya-posle-perenesennoy-koronav .test-cards_item:first-child .test-cards_item-price')
AFTERCOVID_FIRST_CARD_BOOKING_BUTTON = (By.CSS_SELECTOR, '#kompleksnye-issledovaniya-dlya-diagnostiki-covid-19-i-otsenki-sostoyaniya-posle-perenesennoy-koronav .test-cards_item:first-child .test-cards_item-appointment a')

#Catalog
FIRST_PRODUCT_TITLE = (By.XPATH, '(//div[contains(@class, "result-item__row")]//div[contains(@class, "result-item__title")]//a)[1]')
FIRST_PRODUCT_PRICE = (By.XPATH, '(//div[contains(@class, "result-item__row")]//span[contains(@class, "result-item__price")])[1]')
FIRST_PRODUCT_CART_BUTTON = (By.XPATH, '(//div[contains(@class, "result-item__row")]//div[contains(@class, "btn-cart")]//a)[1]')

#Doctors page
FIRST_DOCTOR_NAME = (By.CSS_SELECTOR, '.vrach-card:first-child .vrach-card__person-name')
FIRST_DOCTOR_BOOKING_BUTTON = (By.CSS_SELECTOR, '.vrach-card:first-child .vrach-card__btn')

#Actions page
PAGE_TITLE = (By.CSS_SELECTOR, '.title-block    h1')
FIRST_ACTION = (By.CSS_SELECTOR, '.actions__item:first-child')

#Address page
OFFICES_SEARCH = (By.CSS_SELECTOR, '#offices_page_search')
FILTERS_CHECKBOXES = (By.CSS_SELECTOR, '.offices_sidebar .offices_checkbox')
FIRST_OFFICE_CARD = (By.CSS_SELECTOR, '.offices_card--active:first-child')

#Service page
FIRST_SERVICE_TITLE = (By.CSS_SELECTOR, '[data-id="0"] .show-block-wrap:nth-child(2) .result-item__title')
FIRST_SERVICE_PRICE = (By.CSS_SELECTOR, '[data-id="0"] .show-block-wrap:nth-child(2) .result-item__price')
FIRST_SERVICE_BOOKING_BUTTON = (By.CSS_SELECTOR, '[data-id="0"] .show-block-wrap:nth-child(2) .btn-sign')

#Visit page
NAME_INPUT = (By.CSS_SELECTOR, '#form_text_10')
PHONE_INPUT = (By.CSS_SELECTOR, '#form_text_11')
PERSONAL_DATA_CHECKBOX = (By.CSS_SELECTOR, '[for="id_input1"]')
ORDER_CALL_BUTTON = (By.CSS_SELECTOR, '.ds_b_2x.js-button-submit')

#Programs page
FIRST_HEALTH_PROGRAM = (By.CSS_SELECTOR, '.h-program:first-child')

#Examination page
FIRST_EXAM_TITLE = (By.CSS_SELECTOR, '.show-block-wrap:first-child .result-item__title')
FIRST_EXAM_PRICE = (By.CSS_SELECTOR, '.show-block-wrap:first-child .result-item__price')
FIRST_EXAM_CART_BUTTON = (By.CSS_SELECTOR, '.show-block-wrap:first-child .result-item__col--sm a')

#Results page
CODE_TITLE = (By.XPATH, '//label[text()="Код ИНЗ"]')
BIRTHDAY_TITLE = (By.XPATH, '//label[text()="Дата рождения"]')
LAST_NAME_TITLE = (By.XPATH, '//label[text()="Фамилия"]')
CODE_INPUT = (By.CSS_SELECTOR, 'input[name="orderNumber"]')
BIRTHDAY_INPUT = (By.CSS_SELECTOR, 'input[name="birthday"]')
LAST_NAME_INPUT = (By.CSS_SELECTOR, 'input[name="lastName"]')