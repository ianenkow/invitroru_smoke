from selenium.webdriver.common.by import By

SEARCH_INPUT = (By.CSS_SELECTOR, '.search__input')
FIRST_SEARCH_RESULT_TITLE = (By.CSS_SELECTOR, '.iwg_margin:first-child .search__result-title h3')
CURRENT_CITY_NAME = (By.CSS_SELECTOR, '#city .city__name--label')

COVID19_BUTTON = (By.XPATH, '//a[text()="COVID-19"]')
TESTS_BUTTON = (By.XPATH, '//a[text()="Анализы"]')
BOOKING_BUTTON = (By.XPATH, '//a[text()="Запись к врачу"]')
ACTIONS_BUTTON = (By.XPATH, '//a[text()="Акции"]')
ADDRESS_BUTTON = (By.XPATH, '//a[text()="Адреса"]')
SERVICES_BUTTON = (By.XPATH, '//a[text()="Медицинские услуги"]')
VISIT_BUTTON = (By.XPATH, '//a[text()="Выезд на дом"]')
MORE_MENU = (By.CSS_SELECTOR, '.invitro_header-menu_main-item:last-of-type')
HEALTH_PROGRAMS_ITEM = (By.XPATH, '//div[@id="moreMenuItems"]/a[text()="Программы здоровья"]')
COMPLEX_EXAMINATION_ITEM = (By.XPATH, '//div[@id="moreMenuItems"]/a[text()="Комплексные обследования"]')

TEST_RESULTS_BUTTON = (By.XPATH, '(//a[contains(@class,"invitro_header-get_result")])[1]')

CART_ICON = (By.CSS_SELECTOR, '.invitro_header-middle .invitro-header-cart__icon')
PHONES = (By.CSS_SELECTOR, '#headerPhone a')
LOGIN_ELEMENT = (By.CSS_SELECTOR, '.invitro_header-menu_login')
HAMBURGER_BUTTON = (By.CSS_SELECTOR, '.invitro_header-menu_burger')

#city change locators
CHANGE_CITY_MODAL_WINDOW = (By.CSS_SELECTOR, '#selectBasketCity')
CITY_BUTTON = (By.CSS_SELECTOR, '#city')
CITY_CONFIRM_BUTTON = (By.CSS_SELECTOR, '.city__confirm-btn')
CHANGE_CITY_BUTTON = (By.CSS_SELECTOR, '.city__change-btn')
CITY_SEARCH_INPUT = (By.CSS_SELECTOR, '.change-city-search-input')
FIRST_SEARCH_RESULT = (By.CSS_SELECTOR, '.eac-item:first-child')
