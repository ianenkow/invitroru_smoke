import time
from random import randrange

import pytest
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from locators import (expected_values, footer_loc, header_loc,
                      interactions_loc, visibility_loc)
from pages.actions_page import ActionsPage
from pages.adress_page import AddressPage
from pages.cart_page import CartPage
from pages.catalog_page import CatalogPage
from pages.covid19_page import Covid19Page
from pages.doctors_page import DoctorsPage
from pages.examination_page import ExaminationPage
from pages.main_page import MainPage
from pages.programs_page import ProgramsPage
from pages.results_page import ResultsPage
from pages.services_page import ServicePage
from pages.visit_page import VisitPage


class TestSmokeInvitro:

    '''
    Содержимое хэдера главной страницы соответствует заданному
    '''
    def test_01_header(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        if main_page.browser.find_element(*header_loc.CURRENT_CITY_NAME).text != 'Москва':
            main_page.set_city('Москва')

        header_phone_elements = main_page.browser.find_elements(*header_loc.PHONES)
        actual_header_phones = []

        for phone_element in header_phone_elements:
            actual_header_phones.append(phone_element.text)
            print(phone_element.text)

        assert actual_header_phones == expected_values.header_phones,\
            f'Phone numbers in header are not as expected, actual: {actual_header_phones}'
        assert main_page.browser.find_element(*header_loc.LOGIN_ELEMENT).is_displayed(),\
            'Login button is not displayed'
        assert main_page.browser.find_element(*header_loc.HAMBURGER_BUTTON).is_displayed(),\
            'Hamburger button is not displayed'



    '''
    Содержимое футера главной страницы соответствует заданному
    '''
    def test_02_footer(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        if main_page.browser.find_element(*header_loc.CURRENT_CITY_NAME).text != 'Москва':
            main_page.set_city('Москва')

        footer_phone_elements = main_page.browser.find_elements(*footer_loc.PHONES)
        actual_footer_phones = []
        for phone_element in footer_phone_elements:
            actual_footer_phones.append(phone_element.text)

        footer_site_elements = main_page.browser.find_elements(*footer_loc.SITES_TITLES)
        actual_footer_sites = []
        for site_element in footer_site_elements:
            actual_footer_sites.append(site_element.text)

        footer_group_elements = main_page.browser.find_elements(*footer_loc.GROUPS_TITLES)
        actual_footer_groups = []
        for group_element in footer_group_elements:
            actual_footer_groups.append(group_element.text)

        footer_button_elements = main_page.browser.find_elements(*footer_loc.BUTTON_TITLES)
        actual_footer_buttons = []
        for button_element in footer_button_elements:
            actual_footer_buttons.append(button_element.text)

        assert actual_footer_phones == expected_values.footer_phones
        assert actual_footer_sites == expected_values.footer_sites
        assert actual_footer_groups == expected_values.footer_groups
        assert actual_footer_buttons == expected_values.footer_buttons

    '''
    На главной странице есть разделы "Популярные анализы", "Медицинские услуги", "Врачи Инвитро", "Новости"
    '''
    def test_03_chapters_on_main_page(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        assert main_page.browser.find_element(*visibility_loc.POPULAR_TESTS_TITLE).is_displayed(),\
            'Popular tests element is invisible'

        assert main_page.browser.find_element(*visibility_loc.MEDICAL_SERVICES_TITLE).is_displayed(),\
            'Medical services element is invisible'

        assert main_page.browser.find_element(*visibility_loc.INVITRO_DOCTORS_TITLE).is_displayed(),\
            'Invitro doctors element is invisible'

        assert main_page.browser.find_element(*visibility_loc.NEWS_TITLE).is_displayed(),\
            'News element is invisible'


    '''
    Город меняется корректно
    '''
    def test_04_change_city(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        if not main_page.browser.find_element(*header_loc.CHANGE_CITY_BUTTON).is_displayed():
            main_page.browser.find_element(*header_loc.CITY_BUTTON).click()
        
        main_page.browser.find_element(*header_loc.CHANGE_CITY_BUTTON).click()
        main_page.browser.find_element(*header_loc.CITY_SEARCH_INPUT).send_keys('Волгоград')
        main_page.browser.find_element(*header_loc.FIRST_SEARCH_RESULT).click()

        current_city = main_page.browser.find_element(*header_loc.CURRENT_CITY_NAME).text
        assert current_city == 'Волгоград', f'Wrong city is chosen, {current_city}'


    '''
    Поиск работает корректно
    '''
    def test_05_search(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        text_to_search = 'Анализ крови'
        main_page.browser.find_element(*header_loc.SEARCH_INPUT).send_keys(text_to_search)
        main_page.browser.find_element(*header_loc.SEARCH_INPUT).send_keys(Keys.ENTER)

        assert text_to_search in main_page.browser.find_element(*header_loc.FIRST_SEARCH_RESULT_TITLE).text,'Bad search result'


    '''
    COVID-19 из хэдера открывается (+ видимость заголовка "ПЦР-тесты" и названия, цены и кнопки
"Записаться" у первой карточки)
    '''
    def test_06_COVID19(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        if main_page.browser.find_element(*header_loc.CURRENT_CITY_NAME).text != 'Москва':
            main_page.set_city('Москва')

        main_page.browser.find_element(*header_loc.COVID19_BUTTON).click()
        covid_page = Covid19Page(browser)

        titles_list = covid_page.browser.find_elements(*interactions_loc.TITLES_ELEMENTS)
        for i in range(len(titles_list)):
            assert titles_list[i].text == expected_values.covid19_titles[i], f'actual {titles_list[i].text} expected {expected_values.covid19_titles[i]}'

# Check first card in PCR group is visible
        assert covid_page.browser.find_element(*visibility_loc.PCR_FIRST_CARD_TITLE).is_displayed(), \
            'PCR first card title is not displayed'
        assert covid_page.browser.find_element(*visibility_loc.PCR_FIRST_CARD_PRICE).is_displayed(), \
            'PCR first card price is not displayed'
        assert covid_page.browser.find_element(*visibility_loc.PCR_FIRST_CARD_BOOKING_BUTTON).is_displayed(), 'PCR first card booking button is not displayed'

# Check first card in antibodies group is visible
        assert covid_page.browser.find_element(*visibility_loc.ANTIBODIES_FIRST_CARD_TITLE).is_displayed(),\
            'Antibodies first card title is not displayed'
        assert covid_page.browser.find_element(*visibility_loc.ANTIBODIES_FIRST_CARD_PRICE).is_displayed(),\
            'Antibodies first card price is not displayed'
        assert covid_page.browser.find_element(*visibility_loc.ANTIBODIES_FIRST_CARD_BOOKING_BUTTON).is_displayed(), 'Antibodies first card booking button is not displayed'

# Check first card in aftercovid group is visible
        assert covid_page.browser.find_element(*visibility_loc.AFTERCOVID_FIRST_CARD_TITLE).is_displayed(),\
            'Aftercovid first card title is not displayed'
        assert covid_page.browser.find_element(*visibility_loc.AFTERCOVID_FIRST_CARD_PRICE).is_displayed(),\
            'Aftercovid first card price is not displayed'
        assert covid_page.browser.find_element(*visibility_loc.AFTERCOVID_FIRST_CARD_BOOKING_BUTTON).is_displayed(), 'Aftercovid first card booking button is not displayed'


    '''
    Каталог анализов открывается (+ видимость меню слева, в рандомной категории у первого продукта видимость названия, цены, кнопки "Добавить в корзину" )
    '''
    def test_07_tests_catalog(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        main_page.browser.find_element(*header_loc.TESTS_BUTTON).click()
        catalog_page = CatalogPage(browser)

        test_groups = catalog_page.browser.find_elements(*interactions_loc.TEST_GROUPS)
        i = randrange(len(test_groups))
        test_groups[i].click()

        assert catalog_page.browser.find_element(*visibility_loc.FIRST_PRODUCT_TITLE).is_displayed(), \
            'First product title is not displayed'
        assert catalog_page.browser.find_element(*visibility_loc.FIRST_PRODUCT_PRICE).is_displayed(), \
            'First product price is not displayed'
        assert catalog_page.browser.find_element(*visibility_loc.FIRST_PRODUCT_CART_BUTTON).is_displayed(),\
            'First product add to cart button is not displayed'


    '''
    Запись к врачу открывается (+ видимость списка специальностей слева, видимость имени врача и кнопки "Записаться)
    '''
    def test_08_doctors_appointment(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        if main_page.browser.find_element(*header_loc.CURRENT_CITY_NAME).text != 'Москва':
            main_page.set_city('Москва')

        main_page.browser.find_element(*header_loc.BOOKING_BUTTON).click()
        doctors_page = DoctorsPage(browser)

        doctor_specializations = doctors_page.browser.find_elements(*interactions_loc.DOCTOR_SPECIALIZATIONS)
        i = randrange(len(doctor_specializations)-1)
        doctor_specializations[i].click()

        assert doctors_page.browser.find_element(*visibility_loc.FIRST_DOCTOR_NAME).is_displayed(),\
            'First doctor\'s name is not displayed'
        
        assert doctors_page.browser.find_element(*visibility_loc.FIRST_DOCTOR_BOOKING_BUTTON).is_displayed(), 'First doctor\'s booking button is not displayed'


    '''
    Акции (+ видимость заголовка "Акции" и элементов картинок акций)
    '''
    def test_09_actions(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        main_page.browser.find_element(*header_loc.ACTIONS_BUTTON).click()
        actions_page = ActionsPage(browser)

        assert actions_page.browser.find_element(*visibility_loc.PAGE_TITLE).is_displayed(),\
            'Actions page title is not displayed'

        assert actions_page.browser.find_element(*visibility_loc.FIRST_ACTION).is_displayed(),\
            'There is no actions on action page'


    '''
    Адреса (+ видимость поля поиска, списка фильтров, элементов карточек офисов)
    '''
    def test_10_address(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        if main_page.browser.find_element(*header_loc.CURRENT_CITY_NAME).text != 'Москва':
            main_page.set_city('Москва')

        main_page.browser.find_element(*header_loc.ADDRESS_BUTTON).click()
        address_page = AddressPage(browser)

        assert address_page.browser.find_element(*visibility_loc.OFFICES_SEARCH).is_displayed(),\
            'Search is not displayed'

        filters = address_page.browser.find_elements(*visibility_loc.FILTERS_CHECKBOXES)
        assert len(filters) > 0, 'There are no filters on the page'

        assert address_page.browser.find_element(*visibility_loc.FIRST_OFFICE_CARD).is_displayed(),\
            'No address cards on page'


    '''
    Медицинские услуги (+ видимость списка слева, в рандомной категории видимость заголовка, цены, кнопки "Записаться")
    '''
    def test_11_services(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        if main_page.browser.find_element(*header_loc.CURRENT_CITY_NAME).text != 'Москва':
            main_page.set_city('Москва')
        
        main_page.browser.find_element(*header_loc.SERVICES_BUTTON).click()
        services_page = ServicePage(browser, wait_timeout=10)

        service_groups = services_page.browser.find_elements(*interactions_loc.SERVICES_GROUPS)
        i = randrange(len(service_groups)-1)
        service_groups[2].click()

        assert services_page.browser.find_element(*visibility_loc.FIRST_SERVICE_TITLE).is_displayed(),\
            'First service title is not displayed'

        assert services_page.browser.find_element(*visibility_loc.FIRST_SERVICE_PRICE).is_displayed(),\
            'First service price is not displayed'

        services_page.wait.until(EC.visibility_of_element_located((visibility_loc.FIRST_SERVICE_BOOKING_BUTTON)))
        assert services_page.browser.find_element(*visibility_loc.FIRST_SERVICE_BOOKING_BUTTON).is_displayed(), 'First service booking button is not displayed'


    '''
    Выезд на дом (+ телефон заказа не пустой, видимость полей "Имя", "Телефон", чекбокса согласия на обработку персональных данных, кнопки "Заказать звонок")
    '''
    def test_12_home_visit(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        if main_page.browser.find_element(*header_loc.CURRENT_CITY_NAME).text != 'Москва':
            main_page.set_city('Москва')

        main_page.browser.find_element(*header_loc.VISIT_BUTTON).click()
        visit_page = VisitPage(browser)

        assert visit_page.browser.find_element(*interactions_loc.PHONE_NUMBER).text != '',\
            'Phone number is blank'

        assert visit_page.browser.find_element(*visibility_loc.NAME_INPUT).is_displayed(),\
            'Name input is not displayed'

        assert visit_page.browser.find_element(*visibility_loc.PHONE_INPUT).is_displayed(),\
            'Phone input is not displayed'

        assert visit_page.browser.find_element(*visibility_loc.PERSONAL_DATA_CHECKBOX).is_displayed(),\
            'Personal data checkbox is not displayed'

        assert visit_page.browser.find_element(*visibility_loc.ORDER_CALL_BUTTON).is_displayed(),\
            'Order phone call button is not displayed'


    '''
    Программы здоровья (+ видимость первого элемента программ)
    '''
    def test_13_health_program(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        more_menu = main_page.browser.find_element(*header_loc.MORE_MENU)
        health_programs_item = main_page.browser.find_element(*header_loc.HEALTH_PROGRAMS_ITEM)

        main_page.actions.move_to_element(more_menu).click(health_programs_item).perform()
        programs_page = ProgramsPage(browser)

        assert programs_page.browser.find_element(*visibility_loc.FIRST_HEALTH_PROGRAM).is_displayed(),\
            'No health programs on programs page'


    '''
    Комплексные обследования (+ видимость списка категорий слева, в рандомной категории видимость заголовка, цены и кнопки "Записаться")
    '''
    def test_14_complex_examination(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        if main_page.browser.find_element(*header_loc.CURRENT_CITY_NAME).text != 'Москва':
            main_page.set_city('Москва')

        more_menu = main_page.browser.find_element(*header_loc.MORE_MENU)
        examination_item = main_page.browser.find_element(*header_loc.COMPLEX_EXAMINATION_ITEM)

        main_page.actions.move_to_element(more_menu).click(examination_item).perform()
        exam_page = ExaminationPage(browser)

        exam_groups = exam_page.browser.find_elements(*interactions_loc.EXAMINATION_GROUPS)
        i = randrange(len(exam_groups))
        exam_groups[i].click()

        assert exam_page.browser.find_element(*visibility_loc.FIRST_EXAM_TITLE).is_displayed(),\
            'First exam title is not displayed'

        assert exam_page.browser.find_element(*visibility_loc.FIRST_EXAM_PRICE).is_displayed(),\
            'First exam price is not displayed'

        assert exam_page.browser.find_element(*visibility_loc.FIRST_EXAM_CART_BUTTON).is_displayed(),\
            'First exam cart button is not displayed'


    '''
    Результаты анализов (+ видимость заголовков и полей ввода "Код ИНЗ", "Дата рождения", "Фамилия")
    '''
    def test_15_test_results(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        main_page.browser.find_element(*header_loc.TEST_RESULTS_BUTTON).click()
        results_page = ResultsPage(browser)

        assert results_page.browser.find_element(*visibility_loc.CODE_TITLE).is_displayed(),\
            'Code title is not displayed'

        assert results_page.browser.find_element(*visibility_loc.BIRTHDAY_TITLE).is_displayed(),\
            'Birthday title is not displayed'

        assert results_page.browser.find_element(*visibility_loc.LAST_NAME_TITLE).is_displayed(),\
            'Last name title is not displayed'

        assert results_page.browser.find_element(*visibility_loc.CODE_INPUT).is_displayed(),\
            'Code input is not displayed'

        assert results_page.browser.find_element(*visibility_loc.BIRTHDAY_INPUT).is_displayed(),\
            'Birthday input is not displayed'

        assert results_page.browser.find_element(*visibility_loc.LAST_NAME_INPUT).is_displayed(),\
            'Last name input is not displayed'


    '''
    Корзина (+ видимость списка товаров и кнопки "Оформить заказ")
    Добавление продукта в корзину, наличие в корзине (берём рандомный продукт из рандомного раздела, сверяем наименование в корзине)
    '''
    def test_16_17_cart(self, browser):
        main_page = MainPage(browser)
        main_page.open()

        if main_page.browser.find_element(*header_loc.CURRENT_CITY_NAME).text != 'Москва':
            main_page.set_city('Москва')

        main_page.browser.find_element(*header_loc.TESTS_BUTTON).click()
        catalog_page = CatalogPage(browser)
        expected_number = catalog_page.browser.find_element(*interactions_loc.FIRST_PRODUCT_NUMBER).text
        catalog_page.browser.find_element(*interactions_loc.FIRST_PRODUCT_CART_BUTTON).click()
        
        catalog_page.browser.find_element(*header_loc.CART_ICON).click()
        cart_page = CartPage(browser)
        product_numbers = cart_page.browser.find_elements(*interactions_loc.PRODUCT_NUMBERS)
        result = False
        numbers = []

        for number in product_numbers:
# Тут удаляем первые два символа, т.к. в тексте элемента это всегда значок № и пробел
            numbers.append(number.text[2:])
            if number.text[2:] == expected_number:
                result = True

        assert result, f'There is no {expected_number} product in cart, {numbers}'


