from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC

from locators import header_loc

from .base_page import BasePage


class MainPage(BasePage):
    
    def __init__(self, browser: webdriver.Chrome, timeout=5, wait_timeout=5):
        super().__init__(browser, timeout, wait_timeout)
        self.url = 'https://www.invitro.ru/'


    def open(self):
        self.browser.get(self.url)


    def set_city(self, city_name):
        if not self.browser.find_element(*header_loc.CHANGE_CITY_BUTTON).is_displayed():
            self.browser.find_element(*header_loc.CITY_BUTTON).click()
        
        self.browser.find_element(*header_loc.CHANGE_CITY_BUTTON).click()
        self.browser.find_element(*header_loc.CITY_SEARCH_INPUT).send_keys(city_name)
        self.browser.find_element(*header_loc.FIRST_SEARCH_RESULT).click()
        self.wait.until_not(EC.visibility_of_element_located((header_loc.CHANGE_CITY_MODAL_WINDOW)))
