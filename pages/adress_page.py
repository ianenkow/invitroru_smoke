from selenium import webdriver

from .base_page import BasePage


class AddressPage(BasePage):

    def __init__(self, browser: webdriver.Chrome, timeout=5, wait_timeout=5):
        super().__init__(browser, timeout, wait_timeout)