from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support.ui import WebDriverWait


class BasePage:

    def __init__(self, browser: webdriver.Chrome, timeout=5, wait_timeout=5):
        self.browser = browser
        self.browser.implicitly_wait(timeout)
        self.actions = ActionChains(self.browser)
        self.wait = WebDriverWait(self.browser, wait_timeout)




    
        