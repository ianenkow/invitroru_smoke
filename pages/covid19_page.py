from selenium import webdriver

from pages.base_page import BasePage


class Covid19Page(BasePage):
    
    def __init__(self, browser: webdriver.Chrome, timeout=5, wait_timeout=5):
        super().__init__(browser, timeout, wait_timeout)